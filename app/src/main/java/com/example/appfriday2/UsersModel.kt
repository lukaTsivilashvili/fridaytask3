package com.example.appfriday2

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UsersModel(
    val firstName:String,
    val lastName:String,
    val email:String
): Parcelable
