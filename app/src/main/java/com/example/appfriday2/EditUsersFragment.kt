package com.example.appfriday2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.navigation.Navigation
import com.example.appfriday2.databinding.FragmentAllUsersBinding
import com.example.appfriday2.databinding.FragmentEditUsersBinding

class EditUsersFragment : Fragment() {

    private lateinit var binding: FragmentEditUsersBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEditUsersBinding.inflate(inflater, container, false)

        editUsers()

        return binding.root
    }

    private fun editUsers() {

        binding.BTNsave.setOnClickListener {
            val editFirstName = binding.ETeditFirstName.text.toString()
            val editLastName = binding.ETeditLastName.text.toString()
            val editEmail = binding.ETeditEmail.text.toString()
            val editUser = UsersModel(editFirstName, editLastName, editEmail)
            setFragmentResult("editItems", bundleOf("list" to editUser))
            Navigation.findNavController(it).navigateUp()
        }
    }

}