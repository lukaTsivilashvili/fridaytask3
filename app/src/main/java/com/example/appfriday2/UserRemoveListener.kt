package com.example.appfriday2

interface UserRemoveListener {

    fun editUser(position: Int)
}